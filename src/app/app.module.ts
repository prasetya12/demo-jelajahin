import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {StoreModule } from '@ngrx/store'
import {StoreDevtoolsModule} from '@ngrx/store-devtools'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { DestinationComponent } from './pages/destination/destination.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CardComponent } from './components/card/card.component';
import { SliderCardComponent } from './components/slider-card/slider-card.component';
import { DestinantionCardComponent } from './components/destinantion-card/destinantion-card.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { DetailComponent } from './pages/detail/detail.component';
import { BookingFormComponent } from './components/booking-form/booking-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {reducers} from '../app/reducers'
import { LightboxModule } from 'ngx-lightbox';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ReactiveFormsModule } from '@angular/forms';
import { CartViewComponent } from './pages/cart-view/cart-view.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { BlankLayoutComponent } from './layout/blank-layout/blank-layout.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { PaymentSuccessComponent } from './pages/payment-success/payment-success.component'
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http'
import {APIInterceptor} from './interceptors/api.interceptor'
import { productEffects } from '../app/effects/product.effects';
import { bookingEffects } from '../app/effects/booking.effects';

import { EffectsModule } from '@ngrx/effects';
import { CatalogComponent } from './pages/catalog/catalog.component';
import { ProductsComponent } from './pages/products/products.component';
import { AngularFireModule } from 'angularfire2';
import {AngularFirestore, DocumentChangeAction} from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { AngularFireStorage } from 'angularfire2/storage';

import { environment } from 'src/environments/environment';
import { ScrollableDirective } from './scrollable.directive';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { DetailProductComponent } from './pages/detail-product/detail-product.component';
import { CardListComponent } from './pages/card-list/card-list.component';
import { DisplayErrorsComponent } from './components/display-errors/display-errors.component';

import { NewsComponent } from './pages/news/news.component';
import { RegistrasiHomestayComponent } from './pages/registrasi-homestay/registrasi-homestay.component';
import { SuccessSubmitComponent } from './pages/success-submit/success-submit.component';
import { ListRegistrasiComponent } from './pages/list-registrasi/list-registrasi.component';
import { IndonesiaInternationalComponent } from './pages/indonesia-international/indonesia-international.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DestinationComponent, 
    HeaderComponent,
    FooterComponent,
    CardComponent,
    SliderCardComponent,
    DestinantionCardComponent,
    DetailComponent,
    BookingFormComponent,
    CartViewComponent,
    PaymentComponent,
    BlankLayoutComponent,
    MainLayoutComponent,
    PaymentSuccessComponent,
    CatalogComponent,
    ProductsComponent,
    ScrollableDirective,
    LoadingSpinnerComponent,
    DetailProductComponent,
    CardListComponent,
    DisplayErrorsComponent,
    NewsComponent,
    RegistrasiHomestayComponent,
    SuccessSubmitComponent,
    ListRegistrasiComponent,
    IndonesiaInternationalComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument(),
    EffectsModule.forRoot([productEffects,bookingEffects]),
    SlickCarouselModule,
    FormsModule,
    NgbModule,
    LightboxModule,
    SweetAlert2Module,
    ReactiveFormsModule,
    InfiniteScrollModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule


  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: APIInterceptor,
    multi: true,
  },AngularFirestore,AngularFireStorage],
  bootstrap: [AppComponent]
})
export class AppModule { }
