import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-destinantion-card',
  templateUrl: './destinantion-card.component.html',
  styleUrls: ['./destinantion-card.component.scss']
})
export class DestinantionCardComponent implements OnInit {
  @Input() to:string;
  @Input() title:string; 
  @Input() src:string;
  @Input() content:string;


  constructor() { }

  ngOnInit(): void {
  }

}
