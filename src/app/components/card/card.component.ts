import { Component, OnInit,Input } from '@angular/core';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    @Input() to:string
    @Input() title:string
    @Input() src:string
    @Input() desc:string


    constructor() { }

    ngOnInit(): void {

    }

    shortDesc(desc){
	return desc.length>100?desc.slice(0, 100)+'...':desc;
    }

}
