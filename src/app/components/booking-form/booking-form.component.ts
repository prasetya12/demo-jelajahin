import { Component, Input, OnInit } from '@angular/core';
import {NgbDateStruct,NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import {Store} from '@ngrx/store'
import {addCart} from '../../actions/cart.actions'
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.scss']
})
export class BookingFormComponent implements OnInit {
  @Input() ticket
  quantity:number=0
  model: NgbDateStruct;
  placement = 'top';
  selectedTime='07-08';
  minDate=undefined
  closeResult = '';
  modalReference :any;

  
  constructor(private store:Store<any>,private config: NgbDatepickerConfig,private modalService: NgbModal,private router: Router) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
    this.model = this.minDate
   }
   

  ngOnInit(): void {
  }

  plus(){
    this.quantity = this.quantity + 1
  }

  minus(){
    if(this.quantity >0){
      this.quantity = this.quantity -1
    } 
  }

  addToCart(content){
    var datepicker = this.model
    var date = new Date(datepicker.year, datepicker.month - 1, datepicker.day)
    const temp_tiket = Object.assign({}, this.ticket) ;

    temp_tiket.quantity = this.quantity
    temp_tiket.date = date
    temp_tiket.time = this.selectedTime

    this.store.dispatch(new addCart(temp_tiket))
    this.quantity = 0


    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });


  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  formatNumber(number){
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const num = convert.join('.').split('').reverse().join('')
      return num;
  }

  viewCart(){
    this.modalReference.close();
    this.router.navigate(['/cart/view']);
  }

}
