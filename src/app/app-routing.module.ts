import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from '../app/pages/home/home.component'
import {DestinationComponent} from '../app/pages/destination/destination.component'
import {DetailComponent} from '../app/pages/detail/detail.component'
import {CartViewComponent} from '../app/pages/cart-view/cart-view.component'
import {PaymentComponent} from '../app/pages/payment/payment.component'
import {PaymentSuccessComponent} from '../app/pages/payment-success/payment-success.component'
import { CatalogComponent } from './pages/catalog/catalog.component';
import { ProductsComponent } from './pages/products/products.component';
import { DetailProductComponent } from './pages/detail-product/detail-product.component';
import {ExperienceComponent} from './pages/experience/experience.component'
import {NewsComponent} from './pages/news/news.component'
import {RegistrasiHomestayComponent} from './pages/registrasi-homestay/registrasi-homestay.component'
import {SuccessSubmitComponent} from './pages/success-submit/success-submit.component'
import {ListRegistrasiComponent} from './pages/list-registrasi/list-registrasi.component'
import {IndonesiaInternationalComponent} from './pages/indonesia-international/indonesia-international.component'



import {AppComponent} from '../app/app.component'
import {MainLayoutComponent} from '../app/layout/main-layout/main-layout.component'




const routes: Routes = [{
    path:'payment/success',
    component:PaymentSuccessComponent
  },{
    path:'payment/ref/:reference',
    component:PaymentComponent
  },{
    path:'catalog',
    component:CatalogComponent
  },{
    path:'catalog/products',
    component:ProductsComponent
  },{
    path:'detail-product/:id',
    component:DetailProductComponent,
  },{
    path:'experience',
    component:ExperienceComponent,
  },{
  path:'',
  component:MainLayoutComponent,
  children:[{
      path:'',
      component:HomeComponent
    },{
      path:'registrasi-homestay',
      component:RegistrasiHomestayComponent
    },{
      path:'list-registrasi',
      component:ListRegistrasiComponent
    },{
      path:'success-submit',
      component:SuccessSubmitComponent
    },{
      path:'about-us',
      component:NewsComponent
    },{
      path:'indonesia-international-marathon',
      component:IndonesiaInternationalComponent,
    },{
      path:':place',
      component:DestinationComponent
    },{
      path:':place/tiket-masuk/:id',
      component:DetailComponent
    },{
      path:':place/activity/:id',
      component:DetailComponent
    },{
      path:':place/tour-paket/:id',
      component:DetailComponent
    },{
      path:'cart/view',
      component:CartViewComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
