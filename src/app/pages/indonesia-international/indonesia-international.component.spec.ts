import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndonesiaInternationalComponent } from './indonesia-international.component';

describe('IndonesiaInternationalComponent', () => {
  let component: IndonesiaInternationalComponent;
  let fixture: ComponentFixture<IndonesiaInternationalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndonesiaInternationalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndonesiaInternationalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
