import { Component, OnInit } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-indonesia-international',
  templateUrl: './indonesia-international.component.html',
  styleUrls: ['./indonesia-international.component.scss']
})
export class IndonesiaInternationalComponent implements OnInit {
  private _albums:any = [{"src":"https://www.innagroup.co.id/storage/hotel/grand-inna-bali-beach/pendawa-pool.jpg",
    "caption": "Bali",
    "thumb": "https://www.innagroup.co.id/storage/hotel/grand-inna-bali-beach/pendawa-pool.jpg"},{"src":"https://www.innagroup.co.id/storage/hotel/grand-inna-bali-beach/pool-view.jpg",
    "caption": "Bali",
    "thumb": "https://www.innagroup.co.id/storage/hotel/grand-inna-bali-beach/pool-view.jpg"},{"src":"https://www.innagroup.co.id/storage/hotel/inna-bali-beach-resort/banner.jpg",
    "caption": "Bali",
    "thumb": "https://www.innagroup.co.id/storage/hotel/inna-bali-beach-resort/banner.jpg"}];
  constructor(private _lightbox: Lightbox) { }

  ngOnInit(): void {
  }

  open(index: number): void {
    // open lightbox
    this._lightbox.open(this._albums, index);
  }

}
