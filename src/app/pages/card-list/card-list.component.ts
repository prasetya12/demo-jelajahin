import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  @Input() to:string
  @Input() title:string
  @Input() price:string
  @Input() src:string
  @Input() desc:string
  @Input() name:string  

  constructor() { }

  ngOnInit(): void {
  }

  shortDesc(desc){
    return desc.length>500?desc.slice(0, 500)+'...':desc;
  }

}
