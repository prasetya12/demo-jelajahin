import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,FormBuilder,FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { rejects } from 'assert';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import { map, filter, tap,finalize } from 'rxjs/operators';


@Component({
  selector: 'app-registrasi-homestay',
  templateUrl: './registrasi-homestay.component.html',
  styleUrls: ['./registrasi-homestay.component.scss']
})
export class RegistrasiHomestayComponent implements OnInit {
  form: FormGroup;
  uploadPercent: Observable<number>;
  uploadURL: Observable<string>
  file: Blob;
  url_kamar_tidur;
  url_kamar_mandi;
  url_jalanan_depan;
  url_atraksi;

  private dbPath = '/registrasi-homestay';

  constructor(fb: FormBuilder,private router: Router,private firestore: AngularFirestore,
    private afStorage: AngularFireStorage) {
    this.form = fb.group({
      email: ['',Validators.required],
      penginapan:['',Validators.required],
      nama_penginapan:['',Validators.required],
      no_pemilik:['',Validators.required],
      alamat_pemilik:['',Validators.required],
      long_lat:[''],
      kabkot:['',Validators.required],
      kecamatan:['',Validators.required],
      jumlah_ac:['',Validators.required],
      jumlah_non_ac:['',Validators.required],
      harga_ac:['',Validators.required],
      harga_non_ac:['',Validators.required],
      tempat_tidur:fb.array([],Validators.required),
      kamar_mandi:['',Validators.required],
      tipe_toilet:['',Validators.required],
      fasilitas:fb.array([],Validators.required),
      atraksi:[''],
      url_kamar_tidur:[this.url_kamar_tidur,Validators.required],
      url_kamar_mandi:[this.url_kamar_mandi,Validators.required],
      url_jalanan_depan:[this.url_jalanan_depan,Validators.required],
      url_atraksi:[this.url_atraksi]
    });

   }

  ngOnInit(): void {
  }


  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  


  onChangeTempatTidur(e){
    const tempat_tidur: FormArray = this.form.get('tempat_tidur') as FormArray;

    if (e.target.checked) {

      tempat_tidur.push(new FormControl(e.target.value));

    } else {

       const index = tempat_tidur.controls.findIndex(x => x.value === e.target.value);

       tempat_tidur.removeAt(index);

    }
  }

  onChangeFasilitas(e){
    const fasilitas: FormArray = this.form.get('fasilitas') as FormArray;

    if (e.target.checked) {

      fasilitas.push(new FormControl(e.target.value));

    } else {

       const index = fasilitas.controls.findIndex(x => x.value === e.target.value);

       fasilitas.removeAt(index);

    }
  }


  onClickSubmit() {
    if (this.form.valid) {
      this.firestore.collection("registrasi-homestay").add(this.form.value).then(res=>{
    }, err=>{
      console.log(err) 
    })


    this.router.navigate(['success-submit'], { state: this.form.value });

    }else {
      this.validateAllFormFields(this.form); //{7}
    }

    

  }

  validateAllFormFields(formGroup: FormGroup) { 
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);     
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }
  

  upload(event,image_name) {


    const randomId = Math.random().toString(36).substring(7);
    const task = this.afStorage.upload(randomId, event.target.files[0]);
    const storageRef = this.afStorage.ref(randomId);

    task.snapshotChanges().pipe(
        finalize(() => {
          storageRef.getDownloadURL().subscribe(downloadURL => {
            this.form.controls[image_name].setValue(downloadURL)
          }); 
        })
      ).subscribe();
 
  }

}
