import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.scss']
})
export class DetailProductComponent implements OnInit {

  url:any
  path:any
  id:any
  constructor(private sanitizer: DomSanitizer,private router: Router,private route: ActivatedRoute) { 
    this.path = router.url
    var convert = this.path.split('/')
    this.id = convert[2];

    this.url = this.sanitizer.bypassSecurityTrustResourceUrl("https://widget.globaltix.com/user/auth/R006080M1I?product=info&id="+this.id);


  }

  ngOnInit(): void {
    document.cookie = 'cookie2=value2; SameSite=None; none';

  }

}
