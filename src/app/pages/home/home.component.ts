import { Component, OnInit, ElementRef } from '@angular/core';
import data_motogp_daily from '../../../data/motogp_daily.json'
import data_motogp_weekend from '../../../data/motogp_weekend.json'
import data_motogp_4d3n from '../../../data/motogp_4d3n.json'
import data_motogp_hospitality from '../../../data/motogp_hospitality.json'

import data_ttd_bali from '../../../data/ttd_bali.json'
import data_ttd_danautoba from '../../../data/ttd_danautoba.json'
import data_ttd_jogja from '../../../data/ttd_jogja.json'
import data_ttd_labuanbajo from '../../../data/ttd_labuanbajo.json'
import data_ttd_mandalika from '../../../data/ttd_mandalika.json'

import {Meta, Title} from '@angular/platform-browser'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    motogp_daily: any
    motogp_weekend: any
    motogp_4d3n: any
    motogp_hospitality: any
    
    ttd_bali: any
    ttd_danautoba: any
    ttd_jogja: any
    ttd_labuanbajo: any
    ttd_mandalika: any
    
    load_ttd_bali:any = "Load More"
    load_ttd_jogja:any = "Load More"
    load_ttd_danau_toba:any = "Load More"
    load_ttd_labuan_bajo:any = "Load More"
    load_ttd_mandalika:any = "Load More"

    all_data_bali : any;
    all_data_jogja : any;
    all_data_danau_toba : any;
    all_data_labuan_bajo : any;
    all_data_mandalika : any;


    

    slides: string[] = [ "hintrip-banner.png"
			 // , "grandpremium-zone-a1.png"
			 // , "grandpremium-zone-a2.png"
			 // , "grandpremium-zone-a3.png"
			 // , "grandpremium-zone-b1.png"
			 // , "grandpremium-zone-jk1.png"
			 , "PaketQG.png"
			 , "JelajahinRev4-16.jpg"
		       ].map(ea => "/assets/image/slide/" + ea)

  constructor(private meta:Meta , private title:Title,private elRef:ElementRef) {
      this.meta.addTags([
        {name:'description',content:'Home Page Tiket Moto GP Mandalika'},
        {name:'author', content:'jelajahin'},
        {name:'keywords', content:'tiket,motogp,mandalika'}
      ])
      this.all_data_bali = data_ttd_bali;
      this.all_data_jogja = data_ttd_jogja;
      this.all_data_danau_toba = data_ttd_danautoba;
      this.all_data_labuan_bajo = data_ttd_labuanbajo;
      this.all_data_mandalika = data_ttd_mandalika;


      this.motogp_daily = data_motogp_daily
      this.motogp_weekend = data_motogp_weekend
      this.motogp_4d3n = data_motogp_4d3n
      this.motogp_hospitality = data_motogp_hospitality

      this.ttd_bali = data_ttd_bali.slice(0,3)
      this.ttd_jogja = data_ttd_jogja.slice(0,3)
      this.ttd_danautoba = data_ttd_danautoba.slice(0,3)
      this.ttd_labuanbajo = data_ttd_labuanbajo.slice(0,3)
      this.ttd_mandalika = data_ttd_mandalika.slice(0,3)

      var div_list = this.elRef.nativeElement.querySelector('#bali_list');
      
      this.setTitle()

      

      
  }

  ngOnInit(): void {
   
  }

  loadMoreBali(){
    if(this.ttd_bali.length==3){
      this.ttd_bali = data_ttd_bali
      this.load_ttd_bali = "Hide Data"
    }else{
      this.ttd_bali = data_ttd_bali.slice(0,3)
      this.load_ttd_bali = "Load More"
    }
  }

  loadMoreJogja(){
    if(this.ttd_jogja.length==3){
      this.ttd_jogja = data_ttd_jogja
      this.load_ttd_jogja = "Hide Data"
    }else{
      this.ttd_jogja = data_ttd_jogja.slice(0,3)
      this.load_ttd_jogja = "Load More"
    }
  }

  loadMoreLabuanBajo(){
    if(this.ttd_labuanbajo.length==3){
      this.ttd_labuanbajo = data_ttd_labuanbajo
      this.load_ttd_labuan_bajo = "Hide Data"
    }else{
      this.ttd_labuanbajo = data_ttd_labuanbajo.slice(0,3)
      this.load_ttd_labuan_bajo = "Load More"
    }
  }

  loadMoreMandalika(){
    if(this.ttd_mandalika.length==3){
      this.ttd_mandalika = data_ttd_mandalika
      this.load_ttd_mandalika = "Hide Data"
    }else{
      this.ttd_mandalika = data_ttd_mandalika.slice(0,3)
      this.load_ttd_mandalika = "Load More"
    }
  }

  loadMoreDanauToba(){
    if(this.ttd_danautoba.length==3){
      this.ttd_danautoba = data_ttd_danautoba
      this.load_ttd_danau_toba = "Hide Data"
    }else{
      this.ttd_danautoba = data_ttd_danautoba.slice(0,3)
      this.load_ttd_danau_toba = "Load More"
    }
  }

  

  public setTitle(){
    this.title.setTitle('Tiket Moto GP')
  }

  formatNumber(number){
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const num = convert.join('.').split('').reverse().join('')
      // return number.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      return num;
  }

  imagePath(img){
    var url = "https://sg-api.globaltix.com/api/image?name=";

    return url + img ;
  }

    filtersort(data_to_sort, category) {
      return data_to_sort.filter(ea => ea.category == category).sort((ea1, ea2) => ea1.rank - ea2.rank)
  }

  

}
