import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRegistrasiComponent } from './list-registrasi.component';

describe('ListRegistrasiComponent', () => {
  let component: ListRegistrasiComponent;
  let fixture: ComponentFixture<ListRegistrasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListRegistrasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRegistrasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
