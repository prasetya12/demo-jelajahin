import {Action,createSelector} from '@ngrx/store'
import {BookingActionTypes} from '../actions/booking.actions'

const initialState={
    isLoading:false,
    isError:false,
    dataSuccess:null,
    errorMessage:{}
}


export function bookingReducer(state=initialState,action){
    switch(action.type){
        case BookingActionTypes.CREATE_BOOKING:
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case BookingActionTypes.CREATE_BOOKING_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isError:false,
                dataSuccess:action.payload
            }
        case BookingActionTypes.CREATE_BOOKING_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }
        default:
                return state
        
    }
}